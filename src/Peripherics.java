
public interface Peripherics {
	
	/**
	 * Methode allumant la lampe
	 */
	public void allumer();

	/**
	 * Methode eteignant la lampe
	 */
	public void eteindre();
	
	/**
	 * Methode dercrivant une lampe
	 */
	public String toString();

}
