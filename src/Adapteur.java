
public class Adapteur extends Lumiere implements Peripherics {

	public Adapteur(){
		super();
	}

	@Override
	public void allumer() {
		this.changerIntensite(10);
		
	}

	@Override
	public void eteindre() {
		this.changerIntensite(0);
		
	}
	
	
	
}
