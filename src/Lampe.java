
public class Lampe implements Peripherics {
	
	private String nom;
	private boolean allume;

	/**
	 * Constructeur d'une lampe
	 * @param n nom de la lampe
	 */
	public Lampe(String n){
		this.nom = n;
		this.allume = false;
		
	}
	
	/**
	 * Methode allumant la lampe
	 */
	public void allumer(){
		this.allume = true;
		
	}
	
	/**
	 * Methode returnant l'etat de la lampe
	 * @return allume l'etat de la lampe false si eteinte
	 */
	public boolean etat(){
		return allume;
	}
	
	/**
	 * Methode eteignant la lampe
	 */
	public void eteindre(){
		this.allume = false;
	}
	
	/**
	 * Methode dercrivant une lampe
	 */
	public String toString(){
		if(etat() == true){
			return "la lampe "+this.nom+" est allume";			
		}else{
			return "la lampe "+this.nom+" est eteinte";
		}
	}
}

