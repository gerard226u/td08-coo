import java.util.ArrayList;

public class Telecomande {
	
	private ArrayList<Peripherics> listperipherics = new ArrayList<Peripherics>();

	/**
	 * Constructeur vide cr�ant une Telecommande sans lampe
	 */
	public Telecomande(){}

	/**
	 * Methode ajouterLampe
	 * Elle permet d'ajouter une lampe a une Telecommande
	 * @param e
	 */
 

	public void ajouterPeripherics(Peripherics e){
		listperipherics.add(e);
	}
	

	/**
	 * Methode allumerhifi
	 * Permet d'allumer une chaine hifi sur la telecommande seulement si elle existe bien
	 * @param indicehifi permet d'acc�der a la chaine hifi voulu par rapport a son indice
	 */
	public void allumerPeripherics(int indicehifi){
		if(listperipherics.get(indicehifi)!=null){
			listperipherics.get(indicehifi).allumer();
		}
	}
	
	
	
	/**
	 * Methode desactiverlampe
	 * Permet de desactiver une lampe sur la telecommande seulement si elle existe bien
	 * @param indiceLampe permet d'acc�der a la lampe voulu par rapport a son indice
	 */
	public void eteindrePeripherics(int indiceLampe){
		if(listperipherics.get(indiceLampe)!=null){
			listperipherics.get(indiceLampe).eteindre();
		}
	}
	
	
	/**
	 * Methode activerTout
	 * Elle permet d'activer toute les lampes dans la liste
	 * 
	 * De m�me pour la liste de chaine hifi
	 * @param indiceLampe
	 */
	public void activerToutPeripherics(){
			if (!listperipherics.isEmpty()){	
				for(int i =0; i<listperipherics.size();i++){
					listperipherics.get(i).allumer();
				}
		}
	}
	

	
	
	/**
	 * Methode getListlampe
	 * Methode retournant la liste
	 * @return listlampe
	 */
	public ArrayList getListPeripherics(){
		return listperipherics;
	}
	

	/**
	 * Methode toString 
	 * Methode retournant les proprietes d'une telecommande
	 */
	public String toString(){
		return "La telecommande poss�de : " + listperipherics.size() + " Appareils";
		
	}
	
}
