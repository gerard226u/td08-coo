import static org.junit.Assert.*;

import org.junit.Test;

import thermos.Thermostat;

/**
 * Classe de test pour la telecommande
 */
public class TelecommandeTest {

	/**
	 * methode testant l'ajout d'une lampe quand la liste est vide
	 */
	@Test
	public void testAjouterLampe_telecommandeVide(){
		
		//initialisation
		Telecomande t = new Telecomande();    
		Lampe lampe1 = new Lampe("lampe1");
		
		//methode a tester
		t.ajouterPeripherics(lampe1);
		int size = t.getListPeripherics().size();
		
		//verification
		assertEquals("La liste devrait contenir une lampe",1,size);
		
	}
	
	/**
	 * methode testant l'ajout d'une lampe quand il y en a d�j� une dans la liste
	 */
	@Test
	public void testAjouterLampe_telecommandeRemplit(){
		
		//initialisation
		Telecomande t = new Telecomande();
		Lampe lampe1 = new Lampe("lampe1");
		Lampe lampe2 = new Lampe("lampe2");
		
		t.ajouterPeripherics(lampe1);

		//methode a tester
		t.ajouterPeripherics(lampe2);
		int size = t.getListPeripherics().size();
		//verification
		assertEquals("La liste devrait contenir deux lampe",2,size);
		
	}
	
	/**
	 * methode testant l'allumage a la place 0 de la liste
	 */
	@Test
	public void testActivationLampe_PlaceZero(){
		
		//initialisation
		Telecomande t = new Telecomande();
		Lampe lampe1 = new Lampe("lampe1");
		
		t.ajouterPeripherics(lampe1);

		//methode a tester
		
		t.allumerPeripherics(0);
		
		//verification
		assertEquals("la lampe en position 0 devrait etre allumer",true,lampe1.etat());
		
	}
	
	/**
	 * methode testant l'extinction d'une lampe a la place 0 de la liste
	 */
	@Test
	public void testDesactivationLampe_PlaceZero(){
		
		//initialisation
		Telecomande t = new Telecomande();
		Lampe lampe1 = new Lampe("lampe1");
		
		t.ajouterPeripherics(lampe1);
		t.activerToutPeripherics();
		//methode a tester
		
		t.eteindrePeripherics(0);
		
		//verification
		assertEquals("la lampe en position 0 devrait etre eteinte",false,lampe1.etat());
		
	}
	
	/**
	 * methode testant l'extinction d'une lampe a la place 1 de la liste
	 */
	@Test
	public void testDesactivationLampe_PlaceUne(){
		
		//initialisation
		Telecomande t = new Telecomande();
		Lampe lampe1 = new Lampe("lampe1");
		Lampe lampe2 = new Lampe("lampe1");
		
		t.ajouterPeripherics(lampe1);
		t.ajouterPeripherics(lampe2);
		t.activerToutPeripherics();
		//methode a tester
		
		t.eteindrePeripherics(1);
		
		//verification
		assertEquals("la lampe en position 0 devrait etre allumer",true,lampe1.etat());
		assertEquals("la lampe en position 1 devrait etre eteinte",false,lampe2.etat());
		
	}
	
	/**
	 * methode testant l'allumage a la place 1 de la liste
	 */
	@Test
	public void testActivationLampe_PositionUne(){
		
		//initialisation
		Telecomande t = new Telecomande();
		Lampe lampe1 = new Lampe("lampe1");
		Lampe lampe2 = new Lampe("lampe2");
		
		t.ajouterPeripherics(lampe1);
		t.ajouterPeripherics(lampe2);

		//methode a tester
		
		t.allumerPeripherics(1);
		//verification		
		assertEquals("la lampe en position 0 devrait etre eteinte",false,lampe1.etat());
		assertEquals("la lampe en position 1 devrait etre allumee",true,lampe2.etat());
	}
	
	@Test
	public void testActivationToutLampe(){
		
		//initialisation
		Telecomande t = new Telecomande();
		Lampe lampe1 = new Lampe("lampe1");
		Lampe lampe2 = new Lampe("lampe2");
		Lampe lampe3 = new Lampe("lampe3");
		
		t.ajouterPeripherics(lampe1);
		t.ajouterPeripherics(lampe2);
		t.ajouterPeripherics(lampe3);

		//methode a tester
		
		t.activerToutPeripherics();
		//verification		
		assertEquals("la lampe en position 0 devrait etre allumee",true,lampe1.etat());
		assertEquals("la lampe en position 1 devrait etre allumee",true,lampe2.etat());
		assertEquals("la lampe en position 2 devrait etre allumee",true,lampe2.etat());
	}

	/**
	 * methode testant le constructeur d'une chaine hifi
	 */
	@Test
	public void testConstructeur(){
		//initialisation
		Hifi h = new Hifi();
		
		assertEquals("La hifi devrait avoir un son de 0",0,h.etat());
		
		
	}
	
	/**
	 * methode testant l'ajout d'une chaine hifi � une t�l�commande
	 */
	@Test
	public void testAjoutHifi(){
		
		//initialisation
		Hifi h = new Hifi();
		Telecomande t = new Telecomande();
		
		//methode a tester
		t.ajouterPeripherics(h);
		
		//verification
		assertEquals("La hifi devrait etre ajouter",1,t.getListPeripherics().size());
		
	}
	
	/**
	 * methode testant l'allumage d'une chaine hifi
	 */
	@Test
	public void allumerHifi(){
		
		//initialisation
		Hifi h = new Hifi();
		Telecomande t = new Telecomande();
		
		t.ajouterPeripherics(h);
		h.allumer();
		
		assertEquals("La hifi devrait etre allumer",10,h.etat());
	}
	
	@Test
	public void testLumiere(){
		//initialisation
		Telecomande t = new Telecomande();
		Adapteur l = new Adapteur();
		//methode
		t.ajouterPeripherics(l);
		//verification
		assertEquals("",1,t.getListPeripherics().size());
	}
	
	@Test
	public void testAllumerLumiere(){
		//initialisation
		Telecomande t = new Telecomande();
		Adapteur l = new Adapteur();
		t.ajouterPeripherics(l);
		//methode
		l.allumer();
		//verification
		assertEquals("",10,l.getLumiere());
	}
	
	@Test
	public void testThermostat(){
		//initialisation
		Telecomande t = new Telecomande();
		AdapterThermo l = new AdapterThermo();
		//methode
		t.ajouterPeripherics(l);
		//verification
		assertEquals("",1,t.getListPeripherics().size());
	}
	
	
}
