import java.util.Scanner;

/**
 * classe qui cree des lampes et une Telecomande associee.
 */

public class Main {

	public static void main(String args[]) {
		// la Telecomande vide
		Telecomande t = new Telecomande();

		// ajoute des objets
		// utilisation de refactor (extract method)
		// pour rendre le code plus clair
		ajouter4Lampes(t);

		// test d'activation
		t.allumerPeripherics(1);
		t.allumerPeripherics(3);

		// gestion du menu
		lancerMenu(t);
	}

	/**
	 * menu qui permet de manipuler une Telecomande
	 * 
	 * @param t
	 *            Telecomande a manipuler
	 */
	private static void lancerMenu(Telecomande t) {
		System.out.println(t);
		Scanner sc = new Scanner(System.in);
		
		boolean fini=false;
		
		// tant qu'il y a des commandes
		while (!fini) {
			
			// demande lampe et commande
			System.out.println("entrer le numero de la lampe");
			int choix = sc.nextInt();
			System.out.println("entrer commande (+/-/exit)");
			String com = sc.nextLine();
			com = sc.nextLine();

			// si la commande est +, on active
			if (com.equals("+")) {
				System.out.println("== activer "+choix+"==");
				t.allumerPeripherics(choix);
			}
			// si la commande est - on descactive
			else if (com.equals("-")) {
				System.out.println("== desactiver "+choix+"==");
				t.eteindrePeripherics(choix);
			}
			// si la commande est exit, on arrete
			else if (com.equals("exit")) {
				System.out.println("== Fin du programme == ");
				fini=true;
			}
			// commande non reconnue
			else {
				System.out.println("commande inconnue");
			}
			
			//affiche l'etat de le Telecomande
			System.out.println(t);
		}
		sc.close();
	}

	/**
	 * creation des objets et ajout dans la Telecomande
	 * 
	 * @param t
	 *            Telecomande dans lequelle on ajoute 4 lampes
	 */
	private static void ajouter4Lampes(Telecomande t) {
		Lampe l1 = new Lampe("Lampe1");
		t.ajouterPeripherics(l1);

		Lampe l2 = new Lampe("Lampe2");
		t.ajouterPeripherics(l2);

		Lampe l3 = new Lampe("Lampe3");
		t.ajouterPeripherics(l3);

		Lampe l4 = new Lampe("Lampe4");
		t.ajouterPeripherics(l4);
	}

}
