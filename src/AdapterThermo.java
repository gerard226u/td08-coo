import thermos.Thermostat;

public class AdapterThermo extends Thermostat implements Peripherics{

	public AdapterThermo() {
		super();
	}
	
	@Override
	public void allumer() {
		this.monterTemperature();
		
	}

	@Override
	public void eteindre() {
		this.baisserTemperature();
		
	}

}
